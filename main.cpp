#include <gtkmm.h>
#include "trayicon.h"

int main(int argc, char *argv[])
{
    Gtk::Main app(&argc, &argv);

    TrayIcon *trayIcon = new TrayIcon();

    Gtk::Main::run();
    delete trayIcon;

    return 0;
}
