//
// Created by bjorn on 6/10/15.
//

#ifndef UNTITLED_SHUTDOWNFRAME_H
#define UNTITLED_SHUTDOWNFRAME_H

#include "gtkmm.h"

class ShutdownFrame : public Gtk::Menu
{
public:
    ShutdownFrame();
protected:

    void onShutdown();
    void onHibernate();

    Gtk::MenuItem *shutdownItem_, *hibernateItem_;

};

#endif //UNTITLED_SHUTDOWNFRAME_H
