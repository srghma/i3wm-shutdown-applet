//
// Created by bjorn on 6/10/15.
//

#include "trayicon.h"
#include "shutdownframe.h"

using namespace std;
using namespace Gtk;

#define ICON_NAME "system-shutdown-symbolic"

TrayIcon::TrayIcon() :
      menu_( manage(new Menu()) ),
      aboutItem_( manage(new ImageMenuItem(Stock::ABOUT)) ),
      quitItem_( manage(new ImageMenuItem(Stock::QUIT)) )
{
    aboutItem_->signal_activate().connect(sigc::mem_fun(*this, &TrayIcon::onAbout));
    quitItem_->signal_activate().connect(sigc::mem_fun(*this, &TrayIcon::onQuit));
    menu_->append(*manage(aboutItem_));
    menu_->append(*manage(quitItem_));

    menu_->show_all_children();
    //Staus icon signals
    signal_popup_menu().connect(sigc::mem_fun(*this, &TrayIcon::onPopup));
    signal_activate().connect(sigc::mem_fun(*this, &TrayIcon::onHideRestore));
    signal_button_press_event().connect(sigc::mem_fun(*this, &TrayIcon::onButton));

    set_icon(ICON_NAME);
}

//TODO
void TrayIcon::onAbout() {}

void TrayIcon::onQuit() { Main::quit(); }

bool TrayIcon::onButton(GdkEventButton* event) {
    button_ = event->button;
    activate_time_ = event->time;
    return false;
}

void TrayIcon::onPopup(guint button, guint32 activate_time) {
    popup_menu_at_position(*menu_, button, activate_time);
}

void TrayIcon::onHideRestore() {
    ShutdownFrame *sh = new ShutdownFrame();
    popup_menu_at_position(*sh, button_, activate_time_);
}

void TrayIcon::set_icon(const Glib::ustring & name){
    Glib::RefPtr<IconTheme> iconTheme = IconTheme::get_default();
    if (not iconTheme->has_icon(name))
        return;

    int size = 16;
    Glib::RefPtr<Gdk::Pixbuf> icon = iconTheme->load_icon(name, size, IconLookupFlags(0));
    set(icon);
}
