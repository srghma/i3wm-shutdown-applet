//
// Created by bjorn on 6/10/15.
//

#include "shutdownframe.h"

using namespace std;

ShutdownFrame::ShutdownFrame() :
        shutdownItem_(Gtk::manage(new Gtk::MenuItem("Shutdown"))),
        hibernateItem_(Gtk::manage(new Gtk::MenuItem("Hibernate")))
{
    shutdownItem_->signal_activate().connect(sigc::mem_fun(*this, &ShutdownFrame::onShutdown));
    hibernateItem_->signal_activate().connect(sigc::mem_fun(*this, &ShutdownFrame::onHibernate));
    append(*Gtk::manage(shutdownItem_));
    append(*Gtk::manage(hibernateItem_));

    show_all_children();
}

void ShutdownFrame::onShutdown()
{
    system("dbus-send --system --print-reply --dest=org.freedesktop.login1 \
           /org/freedesktop/login1 org.freedesktop.login1.Manager.PowerOff \
          boolean:true");
}

void ShutdownFrame::onHibernate()
{
    system("dbus-send --system --print-reply --dest=org.freedesktop.login1 \
           /org/freedesktop/login1 org.freedesktop.login1.Manager.Reboot \
           boolean:true");
}