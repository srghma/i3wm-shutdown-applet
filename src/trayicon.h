//
// Created by bjorn on 6/10/15.
//

#ifndef UNTITLED_TRAYICON_H
#define UNTITLED_TRAYICON_H

#include "gtkmm.h"

class TrayIcon : public Gtk::StatusIcon
{
public:
    TrayIcon();

protected:
    //menu actions
    void onAbout();
    void onQuit();
    //icon actions
    void onPopup(guint button, guint32 activate_time);
    void onHideRestore();
    bool onButton(GdkEventButton* event);

    void set_icon(const Glib::ustring & name);
private:
    Gtk::Menu *menu_;
    Gtk::ImageMenuItem *aboutItem_, *quitItem_;

    guint button_;
    guint32 activate_time_;
};

#endif //UNTITLED_TRAYICON_H
